/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4xml;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import static lab4xml.DOMTraversal.followNode;
import org.w3c.dom.Attr;
import org.w3c.dom.Text;

public class CreateXMLDoc {
     @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public static void main(String[] args) {
         try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();


                Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("profile");
		doc.appendChild(rootElement);

		// set attribute to staff element
		Attr attr = doc.createAttribute("id");
		attr.setValue("5830403969");
		rootElement.setAttributeNode(attr);

		// firstname elements
		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode("Pacharakorn Masang"));
		rootElement.appendChild(name);

		// lastname elements
		Element major = doc.createElement("major");
		major.appendChild(doc.createTextNode("Computer Engineering"));
		rootElement.appendChild(major);

		// nickname elements
		Element area = doc.createElement("area");
		area.appendChild(doc.createTextNode("Software"));
		major.appendChild(area);

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("src/lab4xml/myProfile.xml"));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);

		System.out.println("File saved!");

	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
    }
}
